# GitLab-Runner-DinD Design

Technical design of the product

## Resources

### [Security Groups](cloudformation/security-groups.yaml)

| Security Group Logical Id | Description | Inbound Ports/Sources | Outbound Ports/Sources |
| ------------------------- | ----------- | --------------------- | ---------------------- |
| [GitLabRunnerSG](cloudformation/security-groups.yaml#L13) | Allows GitLab Runner to communicate outbound with other services including GitLab | N/A | Any to Any |
| [LambdaSG](cloudformation/security-groups.yaml#L26) | Allows Lambda to communicate outbound with the GitLab API | N/A | `443` to Any |
