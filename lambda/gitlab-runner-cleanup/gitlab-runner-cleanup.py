# import json
# import urllib.parse
import botocore
import boto3
# import csv
import os

ec2 = boto3.client('ec2')


def lookupInstancesByTag(key, value):
    response = ec2.describe_tags(
        Filters=[
            {
                'Name': 'tag:' + key,
                'Values': [value]
            },
        ]
    )
    instance_list = []
    for resource in response['Tags']:
        if resource['ResourceType'] == 'instance':
            instance_list.append(resource['ResourceId'])
    return instance_list


def lambda_handler(event, context):
    self_arn = context.invoked_function_arn
    lambda_client = boto3.client('lambda')
    response = lambda_client.list_tags(Resource=self_arn)
    for instanceId in lookupInstancesByTag('aws:servicecatalog:provisioningArtifactIdentifier', response['Tags']['aws:servicecatalog:provisioningArtifactIdentifier']):
        child_instance_ids = []
        for child_instance_id in lookupInstancesByTag('Parent', instanceId):
            child_instance_ids.append(child_instance_id)
        print(child_instance_ids)
        response = ec2.terminate_instances(
            InstanceIds=child_instance_ids
        )
    return 'Success!'
