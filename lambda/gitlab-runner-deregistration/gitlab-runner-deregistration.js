const https = require('https');
const contentType = 'application/json';

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

const getRunners = function(get_runners_options) {
    return new Promise((resolve, reject) => {
        var request = https.get(get_runners_options, response => {
            response.setEncoding('utf8');
            if (response.statusCode < 200 || response.statusCode > 299) {
                reject(new Error('Failed to load page, status code: ' + response.statusCode));
            }
            const body = []; // temporary data holder
            // on every content chunk, push it to the data array
            response.on('data', (chunk) => body.push(chunk));
            // we are done, resolve promise with those joined chunks
            response.on('end', () => resolve(body.join('')));
        });
        // handle connection errors of the request
        request.on('error', (err) => reject(err));
    });
};

const removeRunner = function(remove_runner_options, runnerId) {
    return new Promise((resolve, reject) => {
        remove_runner_options['path'] = '/api/v4/runners/' + runnerId;
        const postData = JSON.stringify({'runner_id': runnerId});
        var request = https.request(remove_runner_options, response => {
            response.setEncoding('utf8');
            if (response.statusCode < 200 || response.statusCode > 299) {
                reject(new Error('Failed to load page, status code: ' + response.statusCode));
            }
            const body = []; // temporary data holder
            // on every content chunk, push it to the data array
            response.on('data', (chunk) => body.push(chunk));
            // we are done, resolve promise with those joined chunks
            response.on('end', () => resolve(body.join('')));
        });
        // handle connection errors of the request
        request.on('error', (err) => reject(err));
        request.write(postData);
        request.end();
    });
};

const unspecifiedGitLabAPIError = function(message) {
    this.name = 'UnspecifiedGitLabAPIError';
    this.message = message;
};

const runnerNotFoundError = function(message) {
    this.name = 'RunnerNotFoundError';
    this.message = message;
};

exports.handler = async (event) => {
    var runners = null;
    const message = JSON.parse(event.Records[0].Sns.Message);
    const instanceId = message.EC2InstanceId;
    const runnerName = process.env.GitLabRunnerName;
    const apiToken = process.env.GitLabAPIToken;
    const runnerCompleteName = runnerName + '-' + instanceId;
    const api_host = process.env.GitLabRunnerHost;
    console.log("Removing " + runnerCompleteName + " runner(s)...");

    var get_runners_options = {
        hostname: api_host,
        rejectUnauthorized: false,
        path: '/api/v4/runners/all?scope=active',
        headers: {
            'Content-Type': contentType,
            'PRIVATE-TOKEN': apiToken
        }
    };
    var remove_runner_options = {
        hostname: api_host,
        rejectUnauthorized: false,
        method: 'DELETE',
        headers: {
            'Content-Type': contentType,
            'PRIVATE-TOKEN': apiToken
        }
    };
    
    await getRunners(get_runners_options)
        .then((json) => {
            var jsonObj = JSON.parse(json);
            runners = jsonObj.filter(o => o.description === runnerCompleteName);
            if (typeof runners === 'undefined') {
                runnerNotFoundError.prototype = new Error();
                const error = new runnerNotFoundError('No runner named ' + runnerCompleteName + ' could be found!');
                throw error;
            }
        })
        .catch((err) => {
            console.log(err);
            unspecifiedGitLabAPIError.prototype = new Error();
            const error = new unspecifiedGitLabAPIError('Could not get runners!');
            throw error;
        });

    await asyncForEach(runners, async (runner) => {
        await removeRunner(remove_runner_options, runner.id)
        .then((json) => {
            console.log('Runnner ' + runner.id + ' was successfully removed.');
        })
        .catch((err) => console.error(err));
    });
    return('Success!');
};
